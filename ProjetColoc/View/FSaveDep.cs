﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetColoc.Model;
using ProjetColoc.Dao;

namespace ProjetColoc.View
{
    public partial class FSaveDep : Form
    {
        public FSaveDep() {
            InitializeComponent();
            this.load(new DaoDepense().GetAll());
        }

        private void BtnAjoutDep_Click(object sender,EventArgs e) {
            FEditDep fEditDep = new FEditDep(State.added,LBDep.Items,0);
            fEditDep.Show();
        }

        private void BtnModifDep_Click(object sender,EventArgs e) {
            if(LBDep.SelectedIndex==-1) return;
            int position = LBDep.SelectedIndex;
            FEditDep fEditDep = new FEditDep(State.modified,LBDep.Items,position);
            fEditDep.Show();
        }

        private void BtnSaveDep_Click(object sender,EventArgs e) {
            List<Depense> depenses = new List<Depense>();
            foreach(object o in LBDep.Items) {
                depenses.Add((Depense)o);
            }
            new DaoDepense().SaveChanges(depenses);
            this.load(depenses);
        }

        private void load(List<Depense> depenses) {
            LBDep.Items.Clear();
            foreach(Depense c in depenses) {
                LBDep.Items.Add(c);
            }
        }

        private void btnDeleteDep_Click(object sender,EventArgs e) {
            if(LBDep.SelectedIndex==-1) return;
            int position = LBDep.SelectedIndex;
            ((Depense)LBDep.Items[position]).Remove();
            LBDep.Items[position]=LBDep.Items[position];
        }
    }
}
