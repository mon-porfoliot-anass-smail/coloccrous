DROP DATABASE IF EXISTS Coloc;

CREATE DATABASE Coloc DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
use Coloc ;

DROP TABLE IF EXISTS Depense;
DROP TABLE IF EXISTS Colocataire;

CREATE TABLE Colocataire (
id int NOT NULL AUTO_INCREMENT,  
nom varchar(50) NULL,
prenom varchar(50) NULL,
mail varchar(50) NULL,
telephone int(20) NULL,
PRIMARY KEY (id)
) ENGINE=Innodb;

CREATE TABLE Depense (
idDepense int(11) unsigned NOT NULL AUTO_INCREMENT,
dateDepense dateTime NULL,
description varchar(50) NULL,
justificatif varchar(256) NULL,
montant numeric(10,2) NULL,
reparti boolean DEFAULT 0,
idColocataire int NULL,
PRIMARY KEY (idDepense),
FOREIGN KEY (idColocataire) REFERENCES Colocataire (id) 
) ENGINE=Innodb;
