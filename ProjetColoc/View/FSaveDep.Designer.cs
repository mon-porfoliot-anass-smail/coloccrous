﻿namespace ProjetColoc.View
{
    partial class FSaveDep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LBDep = new System.Windows.Forms.ListBox();
            this.btnAjoutDep = new System.Windows.Forms.Button();
            this.BtnModifDep = new System.Windows.Forms.Button();
            this.btnDeleteDep = new System.Windows.Forms.Button();
            this.BtnSaveDep = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LBDep
            // 
            this.LBDep.FormattingEnabled = true;
            this.LBDep.Location = new System.Drawing.Point(12, 94);
            this.LBDep.Name = "LBDep";
            this.LBDep.Size = new System.Drawing.Size(776, 212);
            this.LBDep.TabIndex = 0;
            // 
            // btnAjoutDep
            // 
            this.btnAjoutDep.Location = new System.Drawing.Point(274, 312);
            this.btnAjoutDep.Name = "btnAjoutDep";
            this.btnAjoutDep.Size = new System.Drawing.Size(75, 23);
            this.btnAjoutDep.TabIndex = 1;
            this.btnAjoutDep.Text = "Ajouter";
            this.btnAjoutDep.UseVisualStyleBackColor = true;
            this.btnAjoutDep.Click += new System.EventHandler(this.BtnAjoutDep_Click);
            // 
            // BtnModifDep
            // 
            this.BtnModifDep.Location = new System.Drawing.Point(355, 312);
            this.BtnModifDep.Name = "BtnModifDep";
            this.BtnModifDep.Size = new System.Drawing.Size(75, 23);
            this.BtnModifDep.TabIndex = 2;
            this.BtnModifDep.Text = "Modifier";
            this.BtnModifDep.UseVisualStyleBackColor = true;
            this.BtnModifDep.Click += new System.EventHandler(this.BtnModifDep_Click);
            // 
            // btnDeleteDep
            // 
            this.btnDeleteDep.Location = new System.Drawing.Point(313, 341);
            this.btnDeleteDep.Name = "btnDeleteDep";
            this.btnDeleteDep.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteDep.TabIndex = 3;
            this.btnDeleteDep.Text = "Supprimer";
            this.btnDeleteDep.UseVisualStyleBackColor = true;
            this.btnDeleteDep.Click += new System.EventHandler(this.btnDeleteDep_Click);
            // 
            // BtnSaveDep
            // 
            this.BtnSaveDep.Location = new System.Drawing.Point(394, 341);
            this.BtnSaveDep.Name = "BtnSaveDep";
            this.BtnSaveDep.Size = new System.Drawing.Size(75, 23);
            this.BtnSaveDep.TabIndex = 4;
            this.BtnSaveDep.Text = "Save";
            this.BtnSaveDep.UseVisualStyleBackColor = true;
            this.BtnSaveDep.Click += new System.EventHandler(this.BtnSaveDep_Click);
            // 
            // FSaveDep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BtnSaveDep);
            this.Controls.Add(this.btnDeleteDep);
            this.Controls.Add(this.BtnModifDep);
            this.Controls.Add(this.btnAjoutDep);
            this.Controls.Add(this.LBDep);
            this.Name = "FSaveDep";
            this.Text = "FSaveDep";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LBDep;
        private System.Windows.Forms.Button btnAjoutDep;
        private System.Windows.Forms.Button BtnModifDep;
        private System.Windows.Forms.Button btnDeleteDep;
        private System.Windows.Forms.Button BtnSaveDep;
    }
}