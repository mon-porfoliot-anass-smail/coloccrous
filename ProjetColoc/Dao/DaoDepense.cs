﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using ProjetColoc.Model;

namespace ProjetColoc.Dao {
    public class DaoDepense {
        string DaoConnexion = "user=root;password=siojjr;server=localhost;database=Coloc";
        public void SaveChanges(List<Depense> depenses) {
            for(int i = 0;i<depenses.Count;i++) {
                Depense depense = depenses[i];
                switch(depense.State) {
                    case State.added:
                        this.insert(depense);
                        break;
                    case State.modified:
                        this.update(depense);
                        break;
                    case State.deleted:
                        this.delete(depense);
                        depenses.Remove(depense);
                        break;
                }
            }
        }

        private void delete(Depense depense) {
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand("delete from Depense where idDepense=@idDepense",cnx)) {
                cmd.Parameters.Add(new MySqlParameter("@idDepense",MySqlDbType.Int32));
                cmd.Parameters["@idDepense"].Value=depense.IdDepense;
                cmd.ExecuteNonQuery();
            }
        }

        private void insert(Depense depense) {
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand("insert into Depense(dateDepense,description,justificatif,montant,reparti,idColocataire) values(@dateDepense,@description,@justificatif,@montant,False,@idColocataire)",cnx)) {
                cmd.Parameters.Add(new MySqlParameter("@dateDepense",MySqlDbType.DateTime));
                cmd.Parameters.Add(new MySqlParameter("@description",MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@justificatif",MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@montant",MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@idColocataire",MySqlDbType.Int32));
                cmd.Parameters["@dateDepense"].Value=depense.Date;
                cmd.Parameters["@description"].Value=depense.Description;
                cmd.Parameters["@justificatif"].Value=depense.Justificatif;
                cmd.Parameters["@montant"].Value=depense.Montant;
                cmd.Parameters["@idColocataire"].Value=depense.IdColocataire;
                cmd.ExecuteNonQuery();
            }
            depense.State=State.unChanged;
        }

        private void update(Depense depense) {
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand("update Depense set dateDepense=@dateDepense, description=@description, justificatif=@justificatif, montant=@montant, reparti=@reparti, idColocataire=@idColocataire where idDepense=@idDepense;",cnx)) {
                cmd.Parameters.Add(new MySqlParameter("@idDepense",MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@dateDepense",MySqlDbType.DateTime));
                cmd.Parameters.Add(new MySqlParameter("@description",MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@justificatif",MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@montant",MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@reparti",MySqlDbType.Binary));
                cmd.Parameters.Add(new MySqlParameter("@idColocataire",MySqlDbType.Int32));
                cmd.Parameters["@idDepense"].Value=depense.IdDepense;
                cmd.Parameters["@dateDepense"].Value=depense.Date;
                cmd.Parameters["@description"].Value=depense.Description;
                cmd.Parameters["@justificatif"].Value=depense.Justificatif;
                cmd.Parameters["@montant"].Value=depense.Montant;
                cmd.ExecuteNonQuery();
            }
            depense.State=State.unChanged;
        }

        public bool toutEstReparti() {
            List<bool> repartitions = new List<bool>();
            bool reparti = true;
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand("select reparti from depense",cnx)) {
                MySqlDataReader rdr = cmd.ExecuteReader();
                while(rdr.Read()) {
                    repartitions.Add(Convert.ToBoolean(rdr["reparti"]));
                }
                rdr.Close();
            }
            cnx.Close();
            for(int i = 0;i<repartitions.Count;i++) {
                if(repartitions[i]==false) {
                    reparti=false;
                    break;
                }
            }
            return reparti;
        }

        public List<Depense> GetAll() {
            List<Depense> depenses = new List<Depense>();
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand("select idDepense,dateDepense,description,justificatif,montant,reparti,idColocataire from Depense;",cnx)) {
                using(MySqlDataReader rdr = cmd.ExecuteReader()) {
                    while(rdr.Read()) {
                        depenses.Add(new Depense(Convert.ToInt32(rdr["idDepense"]),Convert.ToDateTime(rdr["dateDepense"]),rdr["description"].ToString(),rdr["justificatif"].ToString(),Convert.ToDecimal(rdr["montant"]),Convert.ToBoolean(rdr["reparti"]),Convert.ToInt32(rdr["idColocataire"]),State.unChanged));
                    }
                }
                cnx.Close();
            }
            return depenses;
        }

        public List<Depense> GetAll(string tri) {
            string req = "select * from Depense Order By "+tri;
            List<Depense> depenses = new List<Depense>();
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand(req,cnx)) {
                ;
                using(MySqlDataReader rdr = cmd.ExecuteReader()) {
                    while(rdr.Read()) {
                        depenses.Add(new Depense(Convert.ToInt32(rdr["idDepense"]),Convert.ToDateTime(rdr["dateDepense"]),rdr["description"].ToString(),rdr["justificatif"].ToString(),Convert.ToDecimal(rdr["montant"]),Convert.ToBoolean(rdr["reparti"]),Convert.ToInt32(rdr["idColocataire"]),State.unChanged));
                    }
                }
            }
            cnx.Close();
            return depenses;
        }

        public List<Depense> GetDepenseNonReparti() {
            List<Depense> depenses = new List<Depense>();
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand("select idDepense,dateDepense,description,justificatif,montant,reparti,idColocataire from depense where reparti=false;",cnx)) {
                using(MySqlDataReader rdr = cmd.ExecuteReader()) {
                    while(rdr.Read()) {
                        depenses.Add(new Depense(Convert.ToInt32(rdr["idDepense"]),Convert.ToDateTime(rdr["dateDepense"]),rdr["description"].ToString(),rdr["justificatif"].ToString(),Convert.ToDecimal(rdr["montant"]),Convert.ToBoolean(rdr["reparti"]),Convert.ToInt32(rdr["idColocataire"]),State.unChanged));
                    }
                    rdr.Close();
                }
            }
            cnx.Close();
            return depenses;
        }

        public void UpdateReparti(int id) {
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand("update depense set reparti=1 where idDepense=@id;",cnx)) {
                cmd.Parameters.Add(new MySqlParameter("@id",id));
                cmd.ExecuteNonQuery();
            }
            cnx.Close();
        }

        public decimal GetDepenseByColoc(int id) {
            decimal resultat;
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand("select sum(montant) from depense where idColocataire=@id and reparti = false;",cnx)) {
                cmd.Parameters.Add(new MySqlParameter("@id",id));
                resultat=Convert.ToDecimal(cmd.ExecuteScalar());
            }
            cnx.Close();
            return resultat;
        }
    }
}