﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetColoc.Model;
using ProjetColoc.Dao;

namespace ProjetColoc.View
{
    public partial class FEditColoc : Form {
        State state;
        ListBox.ObjectCollection items;
        int position;

        public FEditColoc(State state,ListBox.ObjectCollection items,int position) {
            InitializeComponent();
            this.state=state;
            this.items=items;
            this.position=position;
            switch(state) {
                case State.added:
                    this.Text="Ajout colocataire";
                    break;
                case State.modified:
                    Colocataire colocataire = (Colocataire)items[position];
                    this.TbId.Text=colocataire.Id.ToString();
                    this.TBAjoutNom.Text=colocataire.Nom.ToString();
                    this.TBAjoutPrenom.Text=colocataire.Prénom.ToString();
                    this.TBAjoutMail.Text=colocataire.Mail.ToString();
                    this.TBAjoutNBPhone.Text=colocataire.Téléphone.ToString();
                    this.Text="Modification d'un colocataire";
                    break;
                case State.deleted:
                    this.Text="Suppression d'un colocataire";
                    break;
                case State.unChanged:
                    this.Text="Consultation d'un colocataire";
                    break;
                default:
                    break;
            }
        }

        private void BTNValider_Click(object sender,EventArgs e) {
            switch(this.state) {
                case State.added:
                    items.Add(new Colocataire(TBAjoutNom.Text,TBAjoutPrenom.Text,TBAjoutMail.Text,Convert.ToInt32(TBAjoutNBPhone.Text),State.added));
                    break;
                case State.modified:
                    Colocataire colocataire = (Colocataire)items[this.position];
                    colocataire.Nom=this.TBAjoutNom.Text;
                    colocataire.Prénom=this.TBAjoutPrenom.Text;
                    colocataire.Mail=this.TBAjoutMail.Text;
                    colocataire.Téléphone=Convert.ToInt32(this.TBAjoutNBPhone.Text);
                    colocataire.State=State.modified;
                    items[this.position]=colocataire;
                    break;
                case State.deleted:
                    break;
                case State.unChanged:
                    break;
                default:
                    break;
            }
            this.Close();
        }
    }
}
