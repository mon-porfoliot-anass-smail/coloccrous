﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetColoc.View
{
    public partial class FMenu:Form {
        public FMenu() {
            InitializeComponent();
            this.BtnEditPeriode.Click+=new System.EventHandler(this.BtnEditPeriode_Click);
            this.BtnSaveDep.Click+=new System.EventHandler(this.BtnSaveDep_Click);
            this.BtnEditColocs.Click+=new System.EventHandler(this.BtnSaveColocs_Click);
        }
        private void BtnEditPeriode_Click(object sender,EventArgs e) {
            FSoldePeriode fSoldePeriode = new FSoldePeriode();
            fSoldePeriode.Show();
        }
        private void BtnSaveDep_Click(object sender,EventArgs e) {
            FSaveDep fSaveDep = new FSaveDep();
            fSaveDep.Show();
        }
        private void BtnSaveColocs_Click(object sender,EventArgs e) {
            FSaveColoc fSaveColoc = new FSaveColoc();
            fSaveColoc.Show();
        }
    }
}
