﻿namespace ProjetColoc.View
{
    partial class FMenu
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMenu = new System.Windows.Forms.Label();
            this.BtnEditPeriode = new System.Windows.Forms.Button();
            this.BtnSaveDep = new System.Windows.Forms.Button();
            this.BtnEditColocs = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblMenu
            // 
            this.lblMenu.AutoSize = true;
            this.lblMenu.Location = new System.Drawing.Point(343, 9);
            this.lblMenu.Name = "lblMenu";
            this.lblMenu.Size = new System.Drawing.Size(76, 13);
            this.lblMenu.TabIndex = 0;
            this.lblMenu.Text = "Menu principal";
            // 
            // BtnEditPeriode
            // 
            this.BtnEditPeriode.Location = new System.Drawing.Point(309, 165);
            this.BtnEditPeriode.Name = "BtnEditPeriode";
            this.BtnEditPeriode.Size = new System.Drawing.Size(166, 49);
            this.BtnEditPeriode.TabIndex = 1;
            this.BtnEditPeriode.Text = "Solder une période";
            this.BtnEditPeriode.UseVisualStyleBackColor = true;
            // 
            // BtnSaveDep
            // 
            this.BtnSaveDep.Location = new System.Drawing.Point(309, 220);
            this.BtnSaveDep.Name = "BtnSaveDep";
            this.BtnSaveDep.Size = new System.Drawing.Size(166, 44);
            this.BtnSaveDep.TabIndex = 3;
            this.BtnSaveDep.Text = "Gérer les dépenses";
            this.BtnSaveDep.UseVisualStyleBackColor = true;
            // 
            // BtnEditColocs
            // 
            this.BtnEditColocs.Location = new System.Drawing.Point(309, 270);
            this.BtnEditColocs.Name = "BtnEditColocs";
            this.BtnEditColocs.Size = new System.Drawing.Size(166, 47);
            this.BtnEditColocs.TabIndex = 4;
            this.BtnEditColocs.Text = "Gérer les colocataires";
            this.BtnEditColocs.UseVisualStyleBackColor = true;
            // 
            // FMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BtnEditColocs);
            this.Controls.Add(this.BtnSaveDep);
            this.Controls.Add(this.BtnEditPeriode);
            this.Controls.Add(this.lblMenu);
            this.Name = "FMenu";
            this.Text = "FMenu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMenu;
        private System.Windows.Forms.Button BtnEditPeriode;
        private System.Windows.Forms.Button BtnSaveDep;
        private System.Windows.Forms.Button BtnEditColocs;
    }
}

