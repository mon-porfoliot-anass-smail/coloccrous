﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetColoc.Model;
using ProjetColoc.Dao;

namespace ProjetColoc.View {
    public partial class FSaveColoc : Form {
        public FSaveColoc() {
            InitializeComponent();
            this.BtnAddColoc.Click+=new System.EventHandler(this.BtnAddColoc_Click);
            this.load(new DaoColocataire().GetAll());
        }

        private void btnSave_Click(object sender,System.EventArgs e) {
            List<Colocataire> colocataires = new List<Colocataire>();
            foreach(object o in LBColoc.Items) {
                colocataires.Add((Colocataire)o);
            }
            new DaoColocataire().SaveChanges(colocataires);
            this.load(colocataires);
        }


        private void BtnAddColoc_Click(object sender,EventArgs e) {
            FEditColoc fEditColoc = new FEditColoc(State.added,LBColoc.Items,0);
            fEditColoc.Show();
        }

        private void BtnModifColoc_Click(object sender,EventArgs e) {
            if(LBColoc.SelectedIndex==-1) return;
            int position = LBColoc.SelectedIndex;
            FEditColoc fEditColoc = new FEditColoc(State.modified,LBColoc.Items,position);
            fEditColoc.Show();
        }

        private void load(List<Colocataire> colocataires) {
            LBColoc.Items.Clear();
            foreach(Colocataire c in colocataires) {
                LBColoc.Items.Add(c);
            }
        }

        private void BtnDeletColoc_Click(object sender,EventArgs e) {
            if(LBColoc.SelectedIndex==-1) return;
            int position = LBColoc.SelectedIndex;
            ((Colocataire)LBColoc.Items[position]).Remove();
            LBColoc.Items[position]=LBColoc.Items[position];
        }

        private void BtnSaveColoc_Click(object sender,EventArgs e) {
            List<Colocataire> colocataires = new List<Colocataire>();
            foreach(object o in LBColoc.Items) {
                colocataires.Add((Colocataire)o);
            }
            new DaoColocataire().SaveChanges(colocataires);
            this.load(colocataires);
        }
    }
}
