﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetColoc.Model;
using ProjetColoc.Dao;

namespace ProjetColoc.View
{
    public partial class FEditDep : Form {
        State state;
        int id;
        ListBox.ObjectCollection items;
        int position;


        public FEditDep(State state,ListBox.ObjectCollection items,int position) {
            InitializeComponent();
            this.state=state;
            this.items=items;
            this.position=position;
            switch(state) {
                case State.added:
                    this.Text="Ajout d'une dépense";
                    DaoColocataire colocataire = new DaoColocataire();
                    List<Colocataire> colocataires = colocataire.GetAll();
                    foreach(Colocataire coloc in colocataires) {
                        this.CbColocataire.Items.Add(coloc);
                    }

                    break;
                case State.modified:
                    Depense depense = (Depense)items[position];
                    this.TbAjoutID.Text=depense.IdDepense.ToString();
                    this.DtpDate.Text=depense.Date.ToString();
                    this.TbDesc.Text=depense.Description.ToString();
                    this.TbJustificat.Text=depense.Justificatif.ToString();
                    this.TbMontant.Text=depense.Montant.ToString();
                    this.CbColocataire.Enabled=false;
                    this.Text="Modification d'une dépense";
                    break;
                case State.deleted:
                    this.Text="Suppression d'une dépense";
                    break;
                case State.unChanged:
                    this.Text="Consultation d'une dépense";
                    break;
                default:
                    break;
            }
        }

        private void BtnAjoutJustificatif_Click(object sender,EventArgs e) {
            openFileDialog1.InitialDirectory="C:\\git\\ProjetCrous-Coloc\\images\\justificatif";
            openFileDialog1.Filter="Image Files(*.jpg;*.jpeg;*.png)|*.jpg;*.jpeg;*.png|All files (*.*)|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.OK ) {
                TbJustificat.Text=openFileDialog1.FileName;
            }
        }

        private void BtnValiderDep_Click(object sender,EventArgs e) {
            switch(this.state) {
                case State.added:
                    items.Add(new Depense(DtpDate.Value,TbDesc.Text,TbJustificat.Text,Convert.ToDecimal(TbMontant.Text), id,State.added));
                    break;
                case State.modified:
                    Depense depense = (Depense)items[this.position];
                    depense.IdDepense=Convert.ToInt32(this.TbAjoutID.Text);
                    depense.Date=this.DtpDate.Value;
                    depense.Description=this.TbDesc.Text;
                    depense.Justificatif=this.TbJustificat.Text;
                    depense.Montant=Convert.ToDecimal(this.TbMontant.Text);
                    depense.State=State.modified;
                    items[this.position]=depense;
                    break;
                case State.deleted:
                    break;
                case State.unChanged:
                    break;
                default:
                    break;
            }
            this.Close();
        }

        private void CbColocataire_SelectedIndexChanged_1(object sender,EventArgs e) {
            int id = CbColocataire.SelectedIndex;
            Colocataire colocataire = (Colocataire)this.CbColocataire.Items[id];
            this.id = colocataire.Id;
        }
    }
}
