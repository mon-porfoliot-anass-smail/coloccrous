﻿namespace ProjetColoc.View {
    partial class FEditDep {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing&&(components!=null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TbAjoutID = new System.Windows.Forms.TextBox();
            this.TbDesc = new System.Windows.Forms.TextBox();
            this.BtnValiderDep = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.DtpDate = new System.Windows.Forms.DateTimePicker();
            this.TbMontant = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnAjoutJustificatif = new System.Windows.Forms.Button();
            this.TbJustificat = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label4 = new System.Windows.Forms.Label();
            this.CbColocataire = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TbAjoutID
            // 
            this.TbAjoutID.Location = new System.Drawing.Point(264, 127);
            this.TbAjoutID.Name = "TbAjoutID";
            this.TbAjoutID.ReadOnly = true;
            this.TbAjoutID.Size = new System.Drawing.Size(256, 20);
            this.TbAjoutID.TabIndex = 0;
            // 
            // TbDesc
            // 
            this.TbDesc.Location = new System.Drawing.Point(264, 179);
            this.TbDesc.Name = "TbDesc";
            this.TbDesc.Size = new System.Drawing.Size(254, 20);
            this.TbDesc.TabIndex = 3;
            // 
            // BtnValiderDep
            // 
            this.BtnValiderDep.Location = new System.Drawing.Point(264, 284);
            this.BtnValiderDep.Name = "BtnValiderDep";
            this.BtnValiderDep.Size = new System.Drawing.Size(320, 61);
            this.BtnValiderDep.TabIndex = 4;
            this.BtnValiderDep.Text = "Valider";
            this.BtnValiderDep.UseVisualStyleBackColor = true;
            this.BtnValiderDep.Click += new System.EventHandler(this.BtnValiderDep_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // DtpDate
            // 
            this.DtpDate.CustomFormat = "yyyy-MM-dd";
            this.DtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpDate.Location = new System.Drawing.Point(264, 153);
            this.DtpDate.Name = "DtpDate";
            this.DtpDate.Size = new System.Drawing.Size(320, 20);
            this.DtpDate.TabIndex = 6;
            // 
            // TbMontant
            // 
            this.TbMontant.Location = new System.Drawing.Point(264, 230);
            this.TbMontant.Name = "TbMontant";
            this.TbMontant.Size = new System.Drawing.Size(268, 20);
            this.TbMontant.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(526, 130);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "idDepense";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(524, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Description";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(538, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Montant";
            // 
            // BtnAjoutJustificatif
            // 
            this.BtnAjoutJustificatif.Location = new System.Drawing.Point(475, 203);
            this.BtnAjoutJustificatif.Name = "BtnAjoutJustificatif";
            this.BtnAjoutJustificatif.Size = new System.Drawing.Size(109, 23);
            this.BtnAjoutJustificatif.TabIndex = 5;
            this.BtnAjoutJustificatif.Text = "Ajouter un justificatif";
            this.BtnAjoutJustificatif.UseVisualStyleBackColor = true;
            this.BtnAjoutJustificatif.Click += new System.EventHandler(this.BtnAjoutJustificatif_Click);
            // 
            // TbJustificat
            // 
            this.TbJustificat.Location = new System.Drawing.Point(264, 205);
            this.TbJustificat.Name = "TbJustificat";
            this.TbJustificat.ReadOnly = true;
            this.TbJustificat.Size = new System.Drawing.Size(205, 20);
            this.TbJustificat.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(261, 348);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(196, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "* écrire le montant sous ce format EE,cc";
            // 
            // CbColocataire
            // 
            this.CbColocataire.FormattingEnabled = true;
            this.CbColocataire.Location = new System.Drawing.Point(264, 257);
            this.CbColocataire.Name = "CbColocataire";
            this.CbColocataire.Size = new System.Drawing.Size(214, 21);
            this.CbColocataire.TabIndex = 13;
            this.CbColocataire.SelectedIndexChanged += new System.EventHandler(this.CbColocataire_SelectedIndexChanged_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(484, 265);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Choissisez un coloc";
            // 
            // FEditDep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CbColocataire);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TbJustificat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TbMontant);
            this.Controls.Add(this.DtpDate);
            this.Controls.Add(this.BtnAjoutJustificatif);
            this.Controls.Add(this.BtnValiderDep);
            this.Controls.Add(this.TbDesc);
            this.Controls.Add(this.TbAjoutID);
            this.Name = "FEditDep";
            this.Text = "FAjoutDep";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TbAjoutID;
        private System.Windows.Forms.TextBox TbDesc;
        private System.Windows.Forms.Button BtnValiderDep;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DateTimePicker DtpDate;
        private System.Windows.Forms.TextBox TbMontant;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnAjoutJustificatif;
        private System.Windows.Forms.TextBox TbJustificat;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CbColocataire;
        private System.Windows.Forms.Label label5;
    }
}