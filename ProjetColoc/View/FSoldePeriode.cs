﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetColoc.Dao;
using ProjetColoc.Model;

namespace ProjetColoc.View
{
    public partial class FSoldePeriode : Form
    {
        public FSoldePeriode()
        {
            InitializeComponent();
            DaoDepense dao = new DaoDepense();
            if(dao.toutEstReparti()) {
                this.BtnRepartirMontantFalse.Enabled=false;
                this.LblRepartirLesDepenses.Text="Aucune dépense est à répartir";
            }
            this.BtnRepartirMontantFalse.Click+=BtnRepartirMontantFalse_Click;
        }

        private void BtnRepartirMontantFalse_Click(object sender,EventArgs e) {
            try {
                FBalanceDep fBalanceDep = new FBalanceDep();
                fBalanceDep.Show();
                this.Close();
            }
            catch(Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
