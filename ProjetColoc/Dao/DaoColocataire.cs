﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using ProjetColoc.Model;

namespace ProjetColoc.Dao {
    public class DaoColocataire {
        string DaoConnexion = "user=root;password=siojjr;server=localhost;database=Coloc";
        public void SaveChanges(List<Colocataire> colocataires) {
            for(int i = 0;i<colocataires.Count;i++) {
                Colocataire colocataire = colocataires[i];
                switch(colocataire.State) {
                    case State.added:
                        this.insert(colocataire);
                        break;
                    case State.modified:
                        this.update(colocataire);
                        break;
                    case State.deleted:
                        this.delete(colocataire);
                        colocataires.Remove(colocataire);
                        break;
                }
            }
        }

        private void delete(Colocataire colocataire) {
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand("delete from Colocataire where id=@id",cnx)) {
                cmd.Parameters.Add(new MySqlParameter("@id",MySqlDbType.Int32));
                cmd.Parameters["@id"].Value=colocataire.Id;
                cmd.ExecuteNonQuery();
            }
        }

        private void insert(Colocataire colocataire) {
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
                cnx.Open();
                using(MySqlCommand cmd = new MySqlCommand("insert into Colocataire(nom,prenom,mail,telephone) values(@nom,@prenom,@mail,@telephone)",cnx)) {
                    cmd.Parameters.Add(new MySqlParameter("@nom",MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@prenom",MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@mail",MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@telephone",MySqlDbType.Int32));
                    cmd.Parameters["@nom"].Value=colocataire.Nom;
                    cmd.Parameters["@prenom"].Value=colocataire.Prénom;
                    cmd.Parameters["@mail"].Value=colocataire.Mail;
                    cmd.Parameters["@telephone"].Value=colocataire.Téléphone;
                    cmd.ExecuteNonQuery();               
                }
                colocataire.State=State.unChanged;
        }

        private void update(Colocataire colocataire) {
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand("update colocataire set nom=@nom, prenom=@prenom, mail=@mail, telephone=@telephone where id=@id;",cnx)) {
                cmd.Parameters.Add(new MySqlParameter("@id",MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@nom",MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@prenom",MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@mail",MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@telephone",MySqlDbType.Int32));
                cmd.Parameters["@id"].Value=colocataire.Id;
                cmd.Parameters["@nom"].Value=colocataire.Nom;
                cmd.Parameters["@prenom"].Value=colocataire.Prénom;
                cmd.Parameters["@mail"].Value=colocataire.Mail;
                cmd.Parameters["@telephone"].Value=colocataire.Téléphone;
                cmd.ExecuteNonQuery();
            }
            colocataire.State=State.unChanged;
        }

        public List<Colocataire> GetAll() {
            List<Colocataire> colocataires = new List<Colocataire>();
            MySqlConnection cnx = new MySqlConnection(DaoConnexion);
            cnx.Open();
            using(MySqlCommand cmd = new MySqlCommand("select id,nom,prenom,mail,telephone from Colocataire",cnx)) {
                using(MySqlDataReader rdr = cmd.ExecuteReader()) {
                    while(rdr.Read()) {
                        colocataires.Add(new Colocataire(Convert.ToInt32(rdr["id"]),rdr["nom"].ToString(),rdr["prenom"].ToString(),rdr["mail"].ToString(),Convert.ToInt32(rdr["telephone"]),State.unChanged));
                    }
                }
            }
            return colocataires;
        }
    }
}
