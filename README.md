## ProjetCrous-Coloc
Le but du projet, est de créer un software capable de répartir des dépenses entre colocataire de maniére équitable en rajoutant des colocataire ainsi que des dépenses via des crud similaire au Tp-UtdlLapins.

```plantuml
@startuml
:colocataire:
package ProjetCrous {
    colocataire-->(Solder une période)
    (Répartir les dépenses)..>(Solder une période):<extends>
    colocataire-->(Gérer une dépense)
    colocataire-->(Gérer un colocataire)
}
@enduml
```

**voici la bdd du projet:**

```sql
DROP DATABASE IF EXISTS Coloc;

CREATE DATABASE Coloc DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
use Coloc ;

DROP TABLE IF EXISTS Depense;
DROP TABLE IF EXISTS Colocataire;
```

```sql
CREATE TABLE Colocataire (
id int NOT NULL AUTO_INCREMENT,  
nom varchar(50) NULL,
prenom varchar(50) NULL,
mail varchar(50) NULL,
telephone int(20) NULL,
PRIMARY KEY (id)
) ENGINE=Innodb;
```

```sql
CREATE TABLE Depense (
idDepense int(11) unsigned NOT NULL AUTO_INCREMENT,
dateDepense dateTime NULL,
description varchar(50) NULL,
justificatif varchar(256) NULL,
montant decimal(10,2) NULL,
reparti boolean DEFAULT 0,
idColocataire int NULL,
PRIMARY KEY (idDepense),
FOREIGN KEY (idColocataire) REFERENCES Colocataire (id) 
) ENGINE=Innodb;
```

# Main menu
Voici le menu principal de mon software:

![Image du menu principal du projet](images/readmeImages/mainMenu.png)

Le software posséde trois fonctions:
1. Solder une période
2. Gérer les dépenses
3. Gérer les colocataires

Pour enrengistrer et appliquer nos changements dans la bdd, nous avons la classe State.

## Gérer les dépenses
dans la page gérer les dépenses.
Nous avons une listbox montrant les dépenses enrengistrer dans la bdd, pour l'instant notre bdd est vide:

![Image de la page gérer les dépenses](images/readmeImages/pageDepense.png)

# ajouter une dépense
pour ajouter une dépense nous devons cliquer sur le bouton ajouter de notre page.

![Image de la page ajouter une dépense](images/readmeImages/editDepAjout.png)

une fois le formulaire remplis, nous allons devoir sauver les changements dans la page précédente nous devrions voir une dépense d'ajouter son statut seras added, il faut cliquer sur save afin de l'enrengistrer dans la bdd et qu'elle reste affiché dans notre ListBox.
voici la bdd avec quelques dépenses de créer:

![Image de la page gérer les dépenses avec des dépenses](images/readmeImages/pageGérerDepenseAvecDepense.png)

**ATTENTION** un colocataire doit exister afin d''assigner son id à une dépense.

# modifier une dépense
afin de modifier une dépense, nous devons cliquer sur une dépense dans la ListBox et cliquer sur modifier son index nous permettras de recupérer les infos de la dépense et l'afficheras dans la page d'ajout qui seras une page de modif de par le state.
*le seul élément non modifiable d'une dépense est, l'id de la dépense assigner de maniére automatique dans la bdd*

![Image de la page modifier une dépense](images/readmeImages/editDepModif.png)

## Gérer les colocataires
la page gérer les colocataires est similaire à la page gérer les dépenses.

![image de la page gérer les colocataires](images/readmeImages/pageG%C3%A9rerColoc.png)

# ajouter un colocataire

une fois le bouton ajouté cliquer, nous allons devoir remplir les TextBox afin de créer un nouveau colocataire.

![image de la page ajouter un colocataire](images/readmeImages/editColocAjout.png)

**ne pas oublier de sauvegarder le nouveau colocataire avec le bouton save de la page gérer les colocataires**
voici la page avec quelques colocataires:

![image de la page gérer les colocataires avec des colocataires](images/readmeImages/pageG%C3%A9rerColocAvecColoc.png)

# modifier un colocataire 
nous devons sélectionner un colocataire puis cliquer sur le bouton modifier, voici un affichage de la page de modification.

![image de la page modif coloc](images/readmeImages/editColocModif.png)

*de même maniére que l'id d'une dépense celle d'un colocataire ne peut être modifier car rajouter automatiquement dans la bdd*

## Suppresion d'un Colocataire ou d'une Dépense
Dans les pages gérer un colocataire et gérer une dépense, nous avons un bouton supprimer.
Une fois cliquer le statut de la dépense ou du colocataire selectionner passe à deleted après cela il suffit juste de cliquer sur le bouton save.

## Solder une période
afin de répartir les dépenses, nous allons devoir depuis le menu cliquer sur le bouton **Solder une période**.
Nous serons amené sur cette page.

![image de la page solder une période](images/readmeImages/solderUneP%C3%A9riode.png)

une fois le bouton cliqué, nous serons sur la page de répartition.

![image de page de répartition](images/readmeImages/balanceDep.png)

une fois le bouton de répartition cliqué nous aurons la ListBox de droite que nous montreras ce que doivent les colocataires et nous pourrons enrengistrer un fichier texte avec les informations de la ListBox de droite.

![image de la page de répartition avec les montants réparti](images/readmeImages/balanceDepTrue.png)

![image de la sauvegarde du fichier](images/readmeImages/saveFileDep.png)

voici l'exemple d'un ancien fichier txt:

![image du fichier txt](images/readmeImages/fichierTxt.png)

les dépenses enfin réparti la page de répartition seras ainsi indisponible à la suite.