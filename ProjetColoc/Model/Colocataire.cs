﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetColoc.Model {
    public class Colocataire {
        private int id;
        private string nom;
        private string prénom;
        private string mail;
        private int téléphone;
        private State state;

        public Colocataire(int id, string nom, string prénom, string mail, int téléphone, State state) {
            this.id=id;
            this.nom=nom;
            this.prénom=prénom;
            this.mail=mail;
            this.téléphone=téléphone;
            this.state=state;
        }
        public Colocataire(string nom, string prénom, string mail, int téléphone, State state) {
            this.nom=nom;
            this.prénom=prénom;
            this.mail=mail;
            this.téléphone=téléphone;
            this.state=state;
        }
        public int Id {
            get { return this.id; }
            set { this.id=value; }
        }
        public string Nom {
            get { return this.nom; }
            set { this.nom=value; }
        }
        public string Prénom {
            get { return this.prénom; }
            set { this.prénom=value; }
        }
        public string Mail {
            get { return this.mail; }
            set { this.mail=value; }
        }
        public int Téléphone {
            get { return this.téléphone; }
            set { this.téléphone=value; }
        }
        public override string ToString() {
            return string.Format("Id:{0} Nom:{1} Prenom:{2} Mail:{3} Telephone:{4} Statut:{5}",this.id,this.nom,this.prénom,this.mail,this.téléphone,this.state);
        }
        public State State {
            get { return this.state; }
            set { this.state=value; }
        }
        public void Remove() {
            this.state=State.deleted;
        }

        public string Afficher(string nom,string prenom,decimal montant) {
            return string.Format("{0} {1} Doit aux autres colocataires un montant de {2}€",nom,prenom,montant);
        }
    }
}
