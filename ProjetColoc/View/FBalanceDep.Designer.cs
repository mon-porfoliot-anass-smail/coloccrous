﻿namespace ProjetColoc.View
{
    partial class FBalanceDep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbDepenseReparti = new System.Windows.Forms.ListBox();
            this.BTNbalanceDep = new System.Windows.Forms.Button();
            this.LbDepenseNonReparti = new System.Windows.Forms.ListBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // LbDepenseReparti
            // 
            this.LbDepenseReparti.FormattingEnabled = true;
            this.LbDepenseReparti.Location = new System.Drawing.Point(467, 118);
            this.LbDepenseReparti.Name = "LbDepenseReparti";
            this.LbDepenseReparti.Size = new System.Drawing.Size(321, 251);
            this.LbDepenseReparti.TabIndex = 0;
            // 
            // BTNbalanceDep
            // 
            this.BTNbalanceDep.Location = new System.Drawing.Point(363, 327);
            this.BTNbalanceDep.Name = "BTNbalanceDep";
            this.BTNbalanceDep.Size = new System.Drawing.Size(98, 42);
            this.BTNbalanceDep.TabIndex = 1;
            this.BTNbalanceDep.Text = "Répartir les dépenses";
            this.BTNbalanceDep.UseVisualStyleBackColor = true;
            this.BTNbalanceDep.Click += new System.EventHandler(this.BTNbalanceDep_Click);
            // 
            // LbDepenseNonReparti
            // 
            this.LbDepenseNonReparti.FormattingEnabled = true;
            this.LbDepenseNonReparti.Location = new System.Drawing.Point(12, 118);
            this.LbDepenseNonReparti.Name = "LbDepenseNonReparti";
            this.LbDepenseNonReparti.Size = new System.Drawing.Size(345, 251);
            this.LbDepenseNonReparti.TabIndex = 2;
            // 
            // FBalanceDep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.LbDepenseNonReparti);
            this.Controls.Add(this.BTNbalanceDep);
            this.Controls.Add(this.LbDepenseReparti);
            this.Name = "FBalanceDep";
            this.Text = "FBalanceDep";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LbDepenseReparti;
        private System.Windows.Forms.Button BTNbalanceDep;
        private System.Windows.Forms.ListBox LbDepenseNonReparti;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}