﻿namespace ProjetColoc.View
{
    partial class FSaveColoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GBColoc = new System.Windows.Forms.GroupBox();
            this.BtnSaveColoc = new System.Windows.Forms.Button();
            this.BtnDeletColoc = new System.Windows.Forms.Button();
            this.BtnModifColoc = new System.Windows.Forms.Button();
            this.BtnAddColoc = new System.Windows.Forms.Button();
            this.LBColoc = new System.Windows.Forms.ListBox();
            this.GBColoc.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBColoc
            // 
            this.GBColoc.Controls.Add(this.BtnSaveColoc);
            this.GBColoc.Controls.Add(this.BtnDeletColoc);
            this.GBColoc.Controls.Add(this.BtnModifColoc);
            this.GBColoc.Controls.Add(this.BtnAddColoc);
            this.GBColoc.Controls.Add(this.LBColoc);
            this.GBColoc.Location = new System.Drawing.Point(124, 12);
            this.GBColoc.Name = "GBColoc";
            this.GBColoc.Size = new System.Drawing.Size(546, 402);
            this.GBColoc.TabIndex = 0;
            this.GBColoc.TabStop = false;
            // 
            // BtnSaveColoc
            // 
            this.BtnSaveColoc.Location = new System.Drawing.Point(249, 271);
            this.BtnSaveColoc.Name = "BtnSaveColoc";
            this.BtnSaveColoc.Size = new System.Drawing.Size(75, 23);
            this.BtnSaveColoc.TabIndex = 4;
            this.BtnSaveColoc.Text = "Save";
            this.BtnSaveColoc.UseVisualStyleBackColor = true;
            this.BtnSaveColoc.Click += new System.EventHandler(this.BtnSaveColoc_Click);
            // 
            // BtnDeletColoc
            // 
            this.BtnDeletColoc.Location = new System.Drawing.Point(168, 271);
            this.BtnDeletColoc.Name = "BtnDeletColoc";
            this.BtnDeletColoc.Size = new System.Drawing.Size(75, 23);
            this.BtnDeletColoc.TabIndex = 3;
            this.BtnDeletColoc.Text = "Supprimer";
            this.BtnDeletColoc.UseVisualStyleBackColor = true;
            this.BtnDeletColoc.Click += new System.EventHandler(this.BtnDeletColoc_Click);
            // 
            // BtnModifColoc
            // 
            this.BtnModifColoc.Location = new System.Drawing.Point(210, 242);
            this.BtnModifColoc.Name = "BtnModifColoc";
            this.BtnModifColoc.Size = new System.Drawing.Size(75, 23);
            this.BtnModifColoc.TabIndex = 2;
            this.BtnModifColoc.Text = "Modifier";
            this.BtnModifColoc.UseVisualStyleBackColor = true;
            this.BtnModifColoc.Click += new System.EventHandler(this.BtnModifColoc_Click);
            // 
            // BtnAddColoc
            // 
            this.BtnAddColoc.Location = new System.Drawing.Point(129, 242);
            this.BtnAddColoc.Name = "BtnAddColoc";
            this.BtnAddColoc.Size = new System.Drawing.Size(75, 23);
            this.BtnAddColoc.TabIndex = 1;
            this.BtnAddColoc.Text = "Ajouter";
            this.BtnAddColoc.UseVisualStyleBackColor = true;
            // 
            // LBColoc
            // 
            this.LBColoc.FormattingEnabled = true;
            this.LBColoc.Location = new System.Drawing.Point(6, 37);
            this.LBColoc.Name = "LBColoc";
            this.LBColoc.Size = new System.Drawing.Size(534, 199);
            this.LBColoc.TabIndex = 0;
            // 
            // FSaveColoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GBColoc);
            this.Name = "FSaveColoc";
            this.Text = "FEditColoc";
            this.GBColoc.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBColoc;
        private System.Windows.Forms.Button BtnDeletColoc;
        private System.Windows.Forms.Button BtnModifColoc;
        private System.Windows.Forms.Button BtnAddColoc;
        private System.Windows.Forms.ListBox LBColoc;
        private System.Windows.Forms.Button BtnSaveColoc;
    }
}