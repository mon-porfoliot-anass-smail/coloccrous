﻿namespace ProjetColoc.View {
    partial class FSoldePeriode {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing&&(components!=null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BtnRepartirMontantFalse = new System.Windows.Forms.Button();
            this.LblRepartirLesDepenses = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LblRepartirLesDepenses);
            this.groupBox1.Controls.Add(this.BtnRepartirMontantFalse);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 426);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // BtnRepartirMontantFalse
            // 
            this.BtnRepartirMontantFalse.Location = new System.Drawing.Point(284, 151);
            this.BtnRepartirMontantFalse.Name = "BtnRepartirMontantFalse";
            this.BtnRepartirMontantFalse.Size = new System.Drawing.Size(192, 107);
            this.BtnRepartirMontantFalse.TabIndex = 1;
            this.BtnRepartirMontantFalse.Text = "Répartir les dépenses non répartie";
            this.BtnRepartirMontantFalse.UseVisualStyleBackColor = true;
            // 
            // LblRepartirLesDepenses
            // 
            this.LblRepartirLesDepenses.AutoSize = true;
            this.LblRepartirLesDepenses.Location = new System.Drawing.Point(330, 16);
            this.LblRepartirLesDepenses.Name = "LblRepartirLesDepenses";
            this.LblRepartirLesDepenses.Size = new System.Drawing.Size(96, 13);
            this.LblRepartirLesDepenses.TabIndex = 2;
            this.LblRepartirLesDepenses.Text = "Solder une période";
            // 
            // FSoldePeriode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox1);
            this.Name = "FSoldePeriode";
            this.Text = "FSoldePeriode";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BtnRepartirMontantFalse;
        private System.Windows.Forms.Label LblRepartirLesDepenses;
    }
}