﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetColoc.Model {
    public class Depense {
        private int idDepense;
        private DateTime date;
        private string description;
        private string justificatif;
        private decimal montant;
        private bool reparti;
        private int idColocataire;
        private State state;

        public Depense(int idDepense,DateTime date,string description,string justificatif,decimal montant,bool reparti,int idColocataire,State state) {
            this.idDepense=idDepense;
            this.date=date;
            this.description=description;
            this.justificatif=justificatif;
            this.montant=montant;
            this.reparti=reparti;
            this.state=state;
            this.idColocataire=idColocataire;
        }
        public Depense(DateTime date,string description,string justificatif,decimal montant,int idColocataire,State state) {
            this.date=date;
            this.description=description;
            this.justificatif=justificatif;
            this.montant=montant;
            this.reparti=false;
            this.state=state;
            this.idColocataire=idColocataire;
        }

        public int IdDepense {
            get { return this.idDepense; }
            set { this.idDepense=value; }
        }

        public int IdColocataire {
            get { return this.idColocataire; }
            set { this.idColocataire=value; }
        }

        public DateTime Date {
            get { return this.date; }
            set { this.date=value; }
        }

        public string Description {
            get { return this.description; }
            set { this.description=value; }
        }

        public string Justificatif {
            get { return this.justificatif; }
            set { this.justificatif=value; }
        }

        public decimal Montant {
            get { return this.montant; }
            set { this.montant=value; }
        }

        public bool Reparti {
            get { return this.reparti; }
            set { this.reparti=value; }
        }

        public override string ToString() {
            return string.Format("IdDep:{0} Date:{1} Montant:{2} Reparti:{3} Statut:{4}",this.idDepense ,this.date, this.montant, this.reparti, this.state);
        }

        public State State {
            get { return this.state; }
            set { this.state=value; }
        }

        public void Remove() {
            this.state=State.deleted;
        }
    }
}
