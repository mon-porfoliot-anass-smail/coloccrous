﻿namespace ProjetColoc.View
{
    partial class FEditColoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TBAjoutNom = new System.Windows.Forms.TextBox();
            this.TBAjoutPrenom = new System.Windows.Forms.TextBox();
            this.TBAjoutMail = new System.Windows.Forms.TextBox();
            this.TBAjoutNBPhone = new System.Windows.Forms.TextBox();
            this.BTNValider = new System.Windows.Forms.Button();
            this.TbId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TBAjoutNom
            // 
            this.TBAjoutNom.Location = new System.Drawing.Point(323, 134);
            this.TBAjoutNom.Name = "TBAjoutNom";
            this.TBAjoutNom.Size = new System.Drawing.Size(100, 20);
            this.TBAjoutNom.TabIndex = 0;
            // 
            // TBAjoutPrenom
            // 
            this.TBAjoutPrenom.Location = new System.Drawing.Point(323, 160);
            this.TBAjoutPrenom.Name = "TBAjoutPrenom";
            this.TBAjoutPrenom.Size = new System.Drawing.Size(100, 20);
            this.TBAjoutPrenom.TabIndex = 1;
            // 
            // TBAjoutMail
            // 
            this.TBAjoutMail.Location = new System.Drawing.Point(323, 186);
            this.TBAjoutMail.Name = "TBAjoutMail";
            this.TBAjoutMail.Size = new System.Drawing.Size(100, 20);
            this.TBAjoutMail.TabIndex = 2;
            // 
            // TBAjoutNBPhone
            // 
            this.TBAjoutNBPhone.Location = new System.Drawing.Point(323, 212);
            this.TBAjoutNBPhone.Name = "TBAjoutNBPhone";
            this.TBAjoutNBPhone.Size = new System.Drawing.Size(100, 20);
            this.TBAjoutNBPhone.TabIndex = 3;
            // 
            // BTNValider
            // 
            this.BTNValider.Location = new System.Drawing.Point(302, 238);
            this.BTNValider.Name = "BTNValider";
            this.BTNValider.Size = new System.Drawing.Size(145, 57);
            this.BTNValider.TabIndex = 4;
            this.BTNValider.Text = "Valider";
            this.BTNValider.UseVisualStyleBackColor = true;
            this.BTNValider.Click += new System.EventHandler(this.BTNValider_Click);
            // 
            // TbId
            // 
            this.TbId.Location = new System.Drawing.Point(323, 108);
            this.TbId.Name = "TbId";
            this.TbId.ReadOnly = true;
            this.TbId.Size = new System.Drawing.Size(100, 20);
            this.TbId.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(429, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(429, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nom";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(430, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Prenom";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(430, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Mail";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(430, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Telephone";
            // 
            // FEditColoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TbId);
            this.Controls.Add(this.BTNValider);
            this.Controls.Add(this.TBAjoutNBPhone);
            this.Controls.Add(this.TBAjoutMail);
            this.Controls.Add(this.TBAjoutPrenom);
            this.Controls.Add(this.TBAjoutNom);
            this.Name = "FEditColoc";
            this.Text = "FAjoutColoc";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TBAjoutNom;
        private System.Windows.Forms.TextBox TBAjoutPrenom;
        private System.Windows.Forms.TextBox TBAjoutMail;
        private System.Windows.Forms.TextBox TBAjoutNBPhone;
        private System.Windows.Forms.Button BTNValider;
        private System.Windows.Forms.TextBox TbId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}